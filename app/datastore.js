module.exports = {
  set currentSong(d) {
    $("#header #title #thumbnail img").attr("src", d.src.replace(".mp3", ".jpg"));
    $("#header #title #text").text(d.name);
  },
  set songsNumber(d) {
    $("#music-player #info").text("You have "+d+" tracks");
  },
  set user(d) {
    if(d.displayName){
      $("#header #username").text(d.displayName);
      $("#header #logout img").show();
    }
  }
};
