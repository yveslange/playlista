var Player = class Player {
  constructor(){
    this.songs = [] // {src: "...mp3", name: "..."}
    this.history = [];
    this.DOM = $("#music-player");
    this.DOMPlayer = $("#music-player #audio");
    this.DOMPlayer[0].volume = 0.2;
    this.DOMPlaylist = $("#music-player #playlist");
    this.counter = -1;
    this.repeatMode = "all";
    this.shuffle = true;

    this._updateSongList(true);

    setInterval( () => {
      this.antiCrash();
    }, 2000);

    this.DOMPlayer[0].onended = () => {
      setTimeout( () => {
        this.next();
      }, 2000);
    }
  }

  /** Initialize the player */
  antiCrash(){
    if(this.DOMPlayer.error){
      console.log("Player error: restarting...");
      this.start();
    }
  }

  /** Play the previous song */
  prev(){
    if (this.history.length > 1){
      this.counter = this.history.pop();
    } else {
      this.counter = this.history[0];
    }
    this.start();
  }

  /** Play the next song */
  next(){
    if(this.shuffle){
      this.counter = this._getShuffleCounter();
    } else if (this.counter < this.songs.length || this.repeatMode == "all"){
      this.counter = (this.counter + 1) % this.songs.length;
    };
    this.start();
    this.history.push( this.counter);
  }

  /** Start the audio player with the song */
  start(){

    var newSong = this.songs[this.counter];
    console.log("Starting", newSong);

    if(newSong){
      DATASTORE.currentSong = newSong;
      this.DOMPlayer.attr('src', newSong.src);
      this.DOMPlayer[0].load();
      this.DOMPlayer[0].play();
      this.DOMPlaylist.find("div").removeAttr("playing");
      var DOMTrack = (this.DOMPlaylist.find("div[index="+this.counter+"]"))[0]
      DOMTrack.setAttribute("playing", true);
      document.title = newSong.name;
      this._updateMedia(newSong.src.replace(".mp3", ".jpg"));
    }
  }

  _getShuffleCounter(){
    var counter;
    do {
      counter = Math.floor(Math.random() * this.songs.length);
    } while (counter == this.history[this.history.length-1]);
    return counter;
  }

  _updateMedia(img){
    if('mediaSession' in navigator){
      navigator.mediaSession.metadata = new MediaMetadata( {
        artwork: [
          { src: img, sizes: '96x96',   type: 'image/png' },
          { src: img, sizes: '128x128', type: 'image/png' },
          { src: img, sizes: '192x192', type: 'image/png' },
          { src: img, sizes: '256x256', type: 'image/png' },
          { src: img, sizes: '384x384', type: 'image/png' },
          { src: img, sizes: '512x512', type: 'image/png' },
        ]
      });
      navigator.mediaSession.setActionHandler('nexttrack', () => { this.next()});
      navigator.mediaSession.setActionHandler('previoustrack', () => { this.prev()});
    }

  }

  /** Refresh song list */
  _updateSongList(start=false){
    $.get({
      url: "/files/list/",
      success: (data) => {
        var result = data.result;
        this.songs = result.map( (src) => {
          return {
            src: src,
            name: src.replace("files/", "").replace(".mp3", "").replace(/_/g, " ")
          }
        });
        this.DOMPlaylist.text("");
        this.songs.forEach( (song, index) => {
          var item = $("<div class='track' index='"+index+"'>" +
            "<img src='"+song.src.replace(".mp3", ".jpg")+"'>" +
            "<div class='track-name'>"+song.name+"</div>"
          );
          this.DOMPlaylist.append(item);
          item.on('click', (e) => {
            var index = e.currentTarget.getAttribute("index");
            this.counter = index;
            this.history.push( index);
            this.start();
          });
        });
        DATASTORE.songsNumber = this.songs.length;
        if(start) {
          console.log("Starting...", this.songs);
          this.next();
        }
      }
    })
  }
}

var Player = module.exports = new Player();

