
/** Youtube-dl */
var DOMYoutubeDL = $("#youtube-dl");
var DOMYoutubeDL_Input = $("#youtube-dl #add input");
var DOMYoutubeDL_AddClose = $("#youtube-dl #add #close");
var DOMYoutubeDL_AddOpen = $("#youtube-dl #add #open");
var DOMYoutubeDL_Downloader = $("#youtube-dl #downloader");
DOMYoutubeDL_Input.on('click', () => { open(); });
DOMYoutubeDL_AddOpen.on('click', () => { open(); });
DOMYoutubeDL_AddClose.on('click', () => { close(); });

var open = function() {
  DOMYoutubeDL_AddClose.fadeIn("fast");
  DOMYoutubeDL_AddOpen.fadeOut("fast");
  DOMYoutubeDL_Downloader.slideDown("slow");
  DOMYoutubeDL.attr("visible", "true");
}

var close = function () {
  DOMYoutubeDL_AddClose.fadeOut("fast");
  DOMYoutubeDL_AddOpen.fadeIn("fast");
  DOMYoutubeDL_Downloader.slideUp("slow");
  DOMYoutubeDL.removeAttr("visible");
}

var download = function(){
  $.get("/dl.php", {url: youtube_url.value});
  DOMYoutubeDL_Input.val("");
  DOMYoutubeDL_AddClose.click();
}

var youtube_url = document.getElementById("youtube_url");
var youtube_url_submit = document.getElementById("youtube_url_submit");
var logs = document.getElementById("logs");
youtube_url_submit.addEventListener("click", () => {
  download();
});
DOMYoutubeDL_Input.keypress( (e) => {
  if(e.which == 13) {
    download();
  }
});

// setInterval( () => {
//   $.get({
//     url: "/logs.txt",
//     success: (data) => {
//       $("#logs").val(data);
//       $("#logs").scrollTop($("#logs")[0].scrollHeight);
//     }
//   });
// }, 2000);
