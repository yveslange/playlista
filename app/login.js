var exp = module.exports = {
  authenticate(user) {
    if (user) {
      // User is signed in.
      var displayName = user.displayName;
      var email = user.email;
      var emailVerified = user.emailVerified;
      var photoURL = user.photoURL;
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      var providerData = user.providerData;

      DATASTORE.user = user;
      $("#app").fadeIn("slow");
    } else {
      console.log("User not logger, redirecting to google authentication page!");
      googleLoginRedirect();
    }
  },
  logout() {
    firebase.auth().signOut().then(function() {
      // Sign-out successful.
    }).catch(function(error) {
      // An error happened.
    });
  }
}

// Redirect user to google authenticate page
var googleLoginRedirect = () => {
  firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then( () => {
    var provider = new firebase.auth.GoogleAuthProvider();
    firebase.auth().signInWithRedirect(provider);
  });
}

// Authenticate the user when the auth state changes
firebase.auth().onAuthStateChanged(function(user) {
  exp.authenticate(user);
  if(firebase.auth().currentUser){
    firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function(idToken) {
      // Send token to your backend via HTTPS
      // ...
      $.get("http://music.lixend.com:3001/token?id="+idToken);
      console.log("Token", token, user);
    }).catch(function(error) {
      // Handle error
    });
  }
});

setTimeout( () => {
  console.log(firebase.auth().currentUser);
}, 2000);

firebase.auth().getRedirectResult().then((result) => {
  if (result.credential) {
    // This gives you a Google Access Token. You can use it to access the Google API.
    var token = result.credential.accessToken;
    // ...
  }
  // The signed-in user info.
  var user = result.user;

});







// old code
var f = () => {
    // return firebase.auth().getRedirectResult().then(function(result) {
    //   if (result.credential) {
    //     // This gives you a Google Access Token. You can use it to access the Google API.
    //     var token = result.credential.accessToken;
    //     // ...
    //     var user = result.user;
    //     console.log("result.user", result.user);
    //   } else {
      // }
      // The signed-in user info.
    /*}).catch(function(error) {
      // Handle Errors here.
      var errorCode = error.code;
      var errorMessage = error.message;
      // The email of the user's account used.
      var email = error.email;
      // The firebase.auth.AuthCredential type that was used.
      var credential = error.credential;
      // ...
    });*/

  // })
  // Add to the database
  db.collection("users").add({
    first: "Yves",
    last: "Lange",
    born: 1815
  }).then( (docRef) => {
    console.log("Document written with ID: ", docRef.id);
  }).catch( (error) => {
    console.warn("Error adding document: ", error);
  });
}

