
document.addEventListener("DOMContentLoaded", function(event) {
  var config = require("./config/config");
  firebase.initializeApp(config.firebase);
  var login = require("./login");
  var Player = require("./player");
  var youtube = require("./youtube");
  DATASTORE = require("./datastore");


  /** HEADER */
  $("#header #logout").on('click', (e) => {
    login.logout();
  });

  /** MUSIC-PLAYER */
  $("#music-player #next").on('click', () => { Player.next(); });
  $("#music-player #prev").on('click', () => { Player.prev(); });
});

