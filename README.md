# Music
Download music from youtube, create playlists and listen it online everywhere !

# Install

- `cd server`
- `npm install`

# Run

- From the root: `node server/index.js`
- Navigate to the URL with your favorite browser
