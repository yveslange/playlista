const fs = require('fs');
const { spawn } = require('child_process');

var download = (url, options=[]) => {
  const cmd = "youtube-dl";
  const template = "../files/%(title)s.%(ext)s";
  return spawn(cmd, [
    '--extract-audio',
    '--audio-format=mp3',
    '--write-thumbnail',
    '--no-playlist',
    '--write-info-json',
    `-o${template}`,
    url,
    ...options
  ]);
};

var getName = (url, callback) => {
  const child_filename = download(url, ["--get-filename"]);
  child_filename.stdout.on('data', (data) => { filename = data; });
  child_filename.on('close', (code) => {
    filename = (filename+"").trim().replace(/\.webm$/g, "");
    callback(filename);
  });
};


const downloadMP3 = (url, res) => {
  getName(url, (filename) => {
    console.log(">>>>", filename);
    if(fs.existsSync(filename+".mp3")){
      res.end("already exists");
      return;
    }

    const child = download(url);
    child.stdout.on('data', function (data) {
      console.log('stdout: ' + data.toString());
      res.write(data.toString());
    });
    child.stderr.on('data', (data) => {
      console.log(`stderr: ${data}`);
      res.write(data.toString());
    });
    child.on('close', (code) => {
      console.log(`child process exited with code ${code}`);
      res.end();
    });
  });
};

module.exports = {
  downloadMP3: downloadMP3
};
