
/** Firebase */
// Initialize Firebaseconst firebaseAdmi



var admin = require("firebase-admin");

var serviceAccount = require("./serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://playlista-d87f0.firebaseio.com"
});

app.get("/token", (req, res) => {
  const id_token = req.query.id;
  // Build Firebase credential with the Google ID token.
  admin.auth().verifyIdToken(id_token)
  .then(function(decodedToken) {
    var uid = decodedToken.uid;
    console.log(uid, decodedToken);
    // ...
  }).catch(function(error) {
    // Handle error
    console.log(error);
  });

  res.send("Welcome");
});

