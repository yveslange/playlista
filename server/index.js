// Custom configuration
const config = require('./config/config');

// Libraries
const fs = require('fs');
const express = require('express');
const cors = require('cors');
const app = express();
const bunyan = require('bunyan');
const log = bunyan.createLogger({
  name: config.app.name,
  level: config.log.level
});
const youtubeDL = require('./youtube-dl');

app.use(cors());

log.debug(`Application '__dirname' is ${__dirname}`);

// Routes
app.use('/', express.static(__dirname + "/../www/build/default/"));
app.use('/files', express.static(__dirname + '/../files'));

app.get('/files/list', (req, res) => {
  fs.readdir(__dirname + "/../files", (err, files) => {
    if(err) { log.error(err); return; }
    var MP3Files = files.filter( (el) => {return el.match(/\.mp3$/);});
    res.json({result: MP3Files});
  });
});

app.get('/download', (req, res) => {
  console.log("Received:", req.query.url);
  res.writeHead(200, { "Content-Type": "text/event-stream", "Cache-control": "no-cache" });
  const url = req.query.url.trim();
  youtubeDL.downloadMP3(url, res);
});


// Express listening
app.listen(config.express.port, config.express.hostname, () => {
  log.info(`${config.app.name.toUpperCase()} listening on ${config.express.hostname}:${config.express.port}!`);
});
