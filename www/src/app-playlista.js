// Libs
import 'underscore/underscore-min.js';

// Polymer
import { PolymerElement, html } from "@polymer/polymer/polymer-element.js";
import { setPassiveTouchGestures, setRootPath } from '@polymer/polymer/lib/utils/settings.js';
import '@polymer/app-layout/app-drawer/app-drawer.js';
import '@polymer/app-layout/app-drawer-layout/app-drawer-layout.js';
import '@polymer/app-layout/app-header/app-header.js';
import '@polymer/app-layout/app-header-layout/app-header-layout.js';
import '@polymer/app-layout/app-scroll-effects/app-scroll-effects.js';
import '@polymer/app-layout/app-toolbar/app-toolbar.js';
import '@polymer/app-route/app-location.js';
import '@polymer/app-route/app-route.js';
import '@polymer/iron-pages/iron-pages.js';
import '@polymer/iron-selector/iron-selector.js';
import '@polymer/paper-icon-button/paper-icon-button.js';
import '@polymer/paper-toggle-button/paper-toggle-button.js';

// Elements
import './elements/app-icons.js';
import './elements/app-login.js';


/**
 * Gesture events like tap and track generated from touch will not be
 * preventable, allowing for better scrolling performance.
 */
setPassiveTouchGestures(true);

setRootPath(Polymer.rootPath);

class AppPlaylista extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          height: 100%;
          max-height: 100%;
        }

        app-drawer-layout:not([narrow]) [drawer-toggle] {
          display: none;
        }

        app-header paper-icon-button {
          --paper-icon-button-ink-color: white;
        }
        app-header {
          background-color: var(--secondary-background-color);
        }
        app-header app-toolbar div[main-title]{
          min-width: 0;
          height: 100%;
          display: flex;
          align-items: center;
        }
        app-header app-toolbar div[main-title] img {
          display: inline-block;
          width: 40px;
          height: 30px;
          padding: 2px;
          margin-right: 10px;
          margin-top: 2px;
        }
        app-header app-toolbar div[main-title] #header-text {
          max-height: 100%;
          font-size: 3.7vw;
          white-space: nowrap;
          overflow: hidden;
          text-overflow: ellipsis;
        }
        @media screen and ( min-width: 600px) {
          app-header app-toolbar div[main-title] #header-text {
            font-size: 24px;
          }
        }

        app-drawer {
          --app-drawer-content-container: {
            background-color: var(--paper-menu-background-color);
            display: flex;
            flex-direction: column;
            overflow: auto;
          }
          -webkit-box-shadow: 2px 0px 6px -4px rgba(0,0,0,0.75);
          -moz-box-shadow: 2px 0px 6px -4px rgba(0,0,0,0.75);
          box-shadow: 2px 0px 6px -4px rgba(0,0,0,0.75);
        }


        app-drawer app-toolbar {
          min-height: 64px;
        }

        .drawer-list {
          margin: 0 16px;
          flex-grow: 1;
        }
        .drawer-list.options {
          flex-grow: 0;
          padding-bottom: 10px;
        }

        .drawer-list .item {
          display: flex;
          border-bottom: 1px solid #efefef;
          line-height: 40px;
          color: var(--text-primary-color);
        }
        .drawer-list .item:hover {
          cursor: pointer;
          background-color: var(--primary-background-color);
        }
        .drawer-list > .item > * {
          flex-grow: 1;
        }

        .drawer-list a {
          display: flex;
          padding: 0 16px;
          text-decoration: none;
          outline: none;
          padding: 0;
        }

        .drawer-list a.iron-selected {
          color: var(--text-primary-color);
          font-weight: bold;
        }

        #pages {
          height: 100%;
          max-height: 100%;
        }
      </style>

      <app-location route="{{route}}" url-space-regex="^[[rootPath]]">
      </app-location>

      <app-route route="{{route}}" pattern="[[rootPath]]:page" data="{{routeData}}" tail="{{subroute}}">
      </app-route>

      <app-drawer-layout fullbleed="" narrow="{{narrow}}">
        <!-- Drawer content -->
        <app-drawer id="drawer" slot="drawer" swipe-open="[[narrow]]">
          <app-toolbar>Playlista Menu</app-toolbar>

          <div class='drawer-list'>
            <app-login id="login" class='item'></app-login>

            <iron-selector selected="[[page]]" attr-for-selected="name" role="navigation">
              <a name="player" class='item' href="[[rootPath]]player">Player</a>
              <a name="playlist" class='item' href="[[rootPath]]playlist">Playlist</a>
              <a name="about" class='item' href="[[rootPath]]about">About</a>
            </iron-selector>
          </div>

          <div class='drawer-list options'>
            <div class='item'>
              <div on-click="_toggleShuffle">Shuffle playlist</div>
              <paper-toggle-button
                  noink
                  id="shuffleBtn"
                  active="{{playerShuffle}}"
                  checked="[[playerShuffle]]"
              ></paper-toggle-button>
            </div>

            <div class='item' on-click='logout'>Logout</div>
          </div>
        </app-drawer>

        <!-- Main content -->
        <app-header-layout has-scrolling-region="">

          <app-header slot="header" condenses="" reveals="" effects="waterfall">
            <app-toolbar>
              <paper-icon-button icon="app-icons:menu" drawer-toggle=""></paper-icon-button>
              <div main-title>
                <img id="header-logo" src="images/logo.png">
                <div id="header-text">Playlista</div>
              </div>
            </app-toolbar>
          </app-header>

         <div id="pages">
            <page-player name="player" song="{{currentSong}}" shuffle="[[playerShuffle]]"></page-player>
            <page-playlist name="playlist"></page-playlist>
            <page-about name="about"></page-about>
            <page-404 name="404"></page-404>
          </pages>
        </app-header-layout>
      </app-drawer-layout>
    `;
  }

  static get properties() {
    return {
      page: {
        type: String,
        reflectToAttribute: true,
        observer: '_pageChanged'
      },
      routeData: Object,
      subroute: Object,

      playerShuffle: {
        value: () => {
          return localStorage.getItem("_shuffle") == "true";
        },
        observer: '_playerShuffleChanged'
      },

      currentSong: {
        type: Object,
        observer: '_currentSongChanged'
      }
    };
  }

  ready() {
    super.ready();

    const DB = firebase.firestore();
    DB.settings({
      timestampsInSnapshots: true
    });
    window.DB = DB;
  }

  static get observers() {
    return [
      '_routePageChanged(routeData.page)'
    ];
  }

  _routePageChanged(page) {
    if (!page) {
      // If no page was found in the route data, page will be an empty string.
      // Default to 'app-player' in that case.
      this.page = 'player';
    } else if (['player', 'about', 'playlist'].indexOf(page) !== -1) {
      this.page = page;
    } else {
      this.page = '404';
    }

    // Close a non-persistent drawer when the page & route are changed.
    if (!this.$.drawer.persistent) {
      this.$.drawer.close();
    }
  }

  _toggleShuffle(){
    this.playerShuffle = !this.playerShuffle;
  }

  _playerShuffleChanged( playerShuffle) {
    localStorage.setItem('_shuffle', playerShuffle);
  }

  _currentSongChanged( currentSong) {
    this.$['header-text'].innerHTML = currentSong.title;
    this.$['header-logo'].src = currentSong.img;
    document.title = "Playlista - "+currentSong.title;
  }

  logout() {
    this.$.login.logout();
  }

  _pageChanged(pageName) {
    // Load page import on demand. Show 404 page if fails
    // Note: `polymer build` doesn't like string concatenation in
    // the import statement, so break it up.
    var pages = this.$.pages.children;
    for(var p of pages) {
      if (p.getAttribute('name') != pageName) { p.style.display = 'none'; }
      else { p.style.display = ''; }
    }
    switch(pageName) {
      case 'player':
        import('./pages/page-player.js');
        break;
      case 'playlist':
        import('./pages/page-playlist.js');
        break;
      case 'about':
        import('./pages/page-about.js');
        break;
      case '404':
        import('./pages/page-404.js');
        break;
    }
  }

}

window.customElements.define('app-playlista', AppPlaylista);
