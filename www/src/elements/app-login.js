import {PolymerElement} from '@polymer/polymer/polymer-element.js';
// Initialize Firebase
var config = {
  apiKey: 'AIzaSyAlzy9OF641gJwVfN_V6VHhsMac-tnI1ig',
  authDomain: 'playlista-d87f0.firebaseapp.com',
  databaseURL: 'https://playlista-d87f0.firebaseio.com',
  projectId: 'playlista-d87f0',
  storageBucket: 'playlista-d87f0.appspot.com',
  messagingSenderId: '523141322385'
};
var firebase = window.firebase;

firebase.initializeApp(config);
class AppLogin extends PolymerElement {
  static get template() {
    return `
    <style>
    :host {
      display: block;
      width: 100%;
    }
    div#container {
      display: flex;
      align-items: center;
    }
    h4 {
      padding: 0px;
      margin: 0px;
    }
    img {
      width: 40px;
      height: 40px;
      margin-right: 10px;
      border-radius: 20px;
    }
    </style>
    <div id="container">
      <img src="[[img]]">
      <h3>[[username]]</h3>
    </div>
    `;
  }
  static get properties() {
    return {
      username: {
        type: String,
        value: () => { return 'Unknown'; }
      },
      img: {
        type: String
      }
    };
  }


  ready() {
    super.ready();
    // Authenticate the user when the auth state changes
    firebase.auth().onAuthStateChanged( (user) => {
      this.authenticate(user);
      if(firebase.auth().currentUser){
        firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then( (idToken) => {
          // Send token to your backend via HTTPS
          // ...
          //$.get('http://music.lixend.com:3001/token?id='+idToken);
          console.log('Token', idToken);
        }).catch( (error) => {
          // Handle error
          console.error(error);
        });
      }
    });
  }

  googleLoginRedirect() {
    firebase.auth().setPersistence(firebase.auth.Auth.Persistence.LOCAL).then( () => {
      var provider = new firebase.auth.GoogleAuthProvider();
      firebase.auth().signInWithRedirect(provider);
    });
  }

  authenticate(user) {
    if (user) {
      // User is signed in.
      var displayName = user.displayName;
      var email = user.email;
      var emailVerified = user.emailVerified;
      var photoURL = user.photoURL;
      var isAnonymous = user.isAnonymous;
      var uid = user.uid;
      var providerData = user.providerData;

      this.username = displayName;
      this.img = photoURL;
      //$("#app").fadeIn("slow");
    } else {
      console.log('User not logged, redirecting to google authentication page!');
      this.googleLoginRedirect();
    }
  }

  logout() {
    firebase.auth().signOut().then(() => {
      // Sign-out successful.
    }).catch((error) => {
      // An error happened.
    });
  }

}
window.customElements.define('app-login', AppLogin);
