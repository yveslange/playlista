import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/paper-button/paper-button.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-ajax/iron-ajax.js';

class PagePlaylist extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          padding: 10px;
          --paper-input-container-color: #777;
        }

        .card {
          margin: 24px;
          padding: 16px;
          color: #757575;
          border-radius: 5px;
          box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.14), 0 1px 5px 0 rgba(0, 0, 0, 0.12), 0 3px 1px -2px rgba(0, 0, 0, 0.2);
        }

        .circle {
          display: inline-block;
          width: 64px;
          height: 64px;
          text-align: center;
          color: #555;
          border-radius: 50%;
          background: #ddd;
          font-size: 30px;
          line-height: 64px;
        }

        h2 {
          margin: 16px 0;
          color: white;
          font-size: 22px;
        }

        paper-input {
          font-family: Roboto;
          font-size: 16px;
        }

        input[list] {
          width: 100%;
          border: none;
          border-bottom: 1px solid var(--paper-input-container-color);
          outline: none;
          font-family: Roboto;
          font-size: 16px;
          background-color: transparent;
          color: var(--paper-input-container-input-color);
        }
        input[list]:focus {
          border-bottom: 2px solid var(--paper-input-container-focus-color);
        }
        paper-button {
          color: white;
        }
        pre#addsong-result {
          font-size: 10px;
          overflow: auto;
        }
      </style>

      <iron-request
        id="addsong-xhr"
        on-progress-changed="_addsongResponse"
      ></iron-request>

      <div class="card">
        <h2>Add a song</h2>
        <paper-input id="addsong-link" on-keyup="_addSong" label="Youtube link (eg: https://www.youtube.com/watch?v=TDcJJYY5sms")></paper-input>
        <input id="addsong-playlist" list="addsong-playlist-data" on-keyup="_addSong" value="Default" placeholder="Choose a playlist">
        <datalist id="addsong-playlist-data">
          <option>Default</option>
          <option>Bavette</option>
          <option>Cannelloni</option>
        </datalist>
        <paper-button on-click="_addSong"><iron-icon icon="app-icons:check"></iron-icon> Add song</paper-button>
        <pre id="addsong-result"></pre>
      </div>
    `;
  }
  static get properties() {
    return {
    };
  }

  ready() {
    super.ready();

    // Testing code
    window.DB.collection('object').onSnapshot( (qs) => {
      qs.forEach( (doc) => {
        console.log(doc.data().hi);
      });
    });
  }

  _addSong(e) {
    e.preventDefault();
    if(e.type == 'keyup' && e.code != 'Enter') {
      return;
    }


    var link = this.$['addsong-link'].value;
    var playlist = this.$['addsong-playlist'].value;

    if(link.trim()){

      this.$['addsong-result'].innerHTML = 'Loading: '+link;

      this.$['addsong-xhr'].send( {
        url: 'http://'+location.hostname+':3050/download?url='+link+'&playlist='+playlist
      });
      this.$['addsong-xhr'].completes.then( (rep) => {
        console.log('completed', rep.response);
        this.$['addsong-result'].innerHTML = rep.response;
      });

    } else {
      console.warn('Please provide at least a link (should check on server side too');
    }
  }

  _addsongResponse( e) {
    var xhr = this.$['addsong-xhr'].xhr;
    this.$['addsong-result'].innerHTML = xhr.response;
  }
}

window.customElements.define('page-playlist', PagePlaylist);
