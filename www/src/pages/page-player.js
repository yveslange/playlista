import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';
import '@polymer/polymer/lib/elements/dom-repeat.js';
import '@polymer/paper-input/paper-input.js';
import '@polymer/iron-ajax/iron-ajax.js';

// Paper-music-player
import 'paper-music-player/paper-music-player.js';

import style from './page-player-style.js';

class PagePlayer extends PolymerElement {
  static get template() {
    // TODO: re-use html`...`
    return `
      <style>
      :host {
        display: flex;
        flex-direction: column;
        overflow: auto;
        width: 100%;
        max-height: 100%;
        height: 100%;
      }

      paper-music-player {
        --paper-music-player-background1: var(--secondary-background-color);
        --paper-music-player-background2: var(--secondary-background-color);
      }
      ${style}
      </style>
      <iron-ajax auto url$="[[_host]]/files/list" on-response="_playlistReceived"></iron-ajax>

      <div id="player-container">
        <paper-music-player
            _debug
            id="player"
            playlist=[[playlist]]
            index="{{currentIndex}}"
            mini
            hide-playlist-btn
        ></paper-music-player>
      </div>
      <div id="content">

        <div id="search-bar">
          <paper-input id="search" label="Search a song…" value="{{search}}"></paper-input>
        </div>

        <div id="playlist">
          <template is="dom-repeat" items=[[playlist]]>
            <div
                class="song"
                on-click="songClicked"
                index$="[[index]]"
                playing$="[[_isIndexPlaying(index, currentIndex)]]"
                filtered$="[[_isSongFiltered(item.title, search)]]">
              <img src=[[item.img]] width=20 height=20>
              <div>[[item.title]]</div>
            </div>
          </template>
        </div>
      </div>

      <!--
        <div id="youtube-downloader">YOUTUBE DL</div>
        <button on-click="foo">Click me</button>
      -->
    `;
  }
  static get properties() {
    return {
      /**
       * The current playlist
       */
      playlist: {
        type: Array,
        observer: '_playlistChanged'
      },

      /**
       * The original playlist (first one received from the server)
       */
      __playlist: {
        type: Array,
        value: () => { return []; }
      },

      /**
       * The current index being played
       */
      currentIndex: {
        observer: '_currentIndexChanged'
      },

      /**
       * The current playling song
       */
      song: {
        type: Object,
        notify: true
      },

      /**
       * Search criteria
       */
      search: {
        type: String,
        value: () => { return ''; }
      },

      /**
       * Host URL
       */
      _host: {
        type: String,
        value: () => { return 'http://'+location.hostname+':3050'; }
      },

      /**
       * Should the playlist be shuffled
       */
      shuffle: {
        type: Boolean,
        observer: '_shuffleChanged'
      }
    };
  }

  ready() {
    super.ready();

    /** Register the <ENTER> key to select the first song found by the search criteria */
    this.$.search.addEventListener('keypress', (e) => {
      if(e.keyCode == 13 && this.search.trim() != '') {
        var song = this.$.playlist.querySelector('.song:not([filtered])');
        if(song) {
          this.search = '';
          this.songClicked({currentTarget: song});
        }
      }
    });
  }

  /** What to do when the playlist is received from the server */
  _playlistReceived( event) {
    var resp = event.detail.response;
    if(resp) {

      setTimeout( () => {
        // Create the playlist collection
        var playlist = resp.result.map( (el) => {
          var title = el.replace(/\.mp3$/g, '').replace(/_/g, ' ');
          return {file: this._host+'/files/'+el, title: title, img: this._host+'/files/'+el.replace('.mp3', '.jpg')};
        });

        // Create an original clone
        this.__playlist = playlist.slice(0);

        // Shuffle the playlist if needed
        if(this.shuffle) {
          playlist = this._shuffle(playlist);
        }

        this.playlist = playlist;

      }, 200);
    }
  }

  /** Updates the current song to index 0 when the playlist changed */
  _playlistChanged(playlist) {
    if(playlist && playlist.length > 0 ) {
      this.song = playlist[0];
    }
  }

  /** Return true if the song should be filtered by the search criteria */
  _isSongFiltered(title, search) {
    search = search.trim();
    if(search == undefined || search == '') { return false; }
    return title.toLowerCase().indexOf(search.toLowerCase()) == -1 ;
  }

  /** Change the title when the current index changed */
  _currentIndexChanged(currentIndex) {
    console.log("Current index changed to:", currentIndex);
    if(this.playlist && this.playlist.length > 0) {
      console.log("Playing index:", currentIndex );
      this.song = this.playlist[currentIndex];
    }
  }

  /** Return true if the index song is currently playling */
  _isIndexPlaying(index, currentIndex) {
    return index == currentIndex;
  }

  /** Shuffle the playlist */
  _shuffleChanged( shuffle) {
    var playlist = this.__playlist.slice(0);

    if(playlist) {
      if(shuffle) {
        playlist = this._shuffle(playlist);
      }
      this.playlist = playlist;
    }
  }

  _shuffle(items){
    items.sort( function() { return 0.5 - Math.random(); });
    return items;
  }

  /** Actions to do when a song is clicked */
  songClicked(e) {
    var index = parseInt(e.currentTarget.getAttribute('index'));
    this.$.player.skipTo(index);
  }
}

window.customElements.define('page-player', PagePlayer);
