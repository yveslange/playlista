/**
 * @license
 * Copyright (c) 2016 The Polymer Project Authors. All rights reserved.
 * This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
 * The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
 * The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
 * Code distributed by Google as part of the polymer project is also
 * subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
 */

import { PolymerElement, html } from '@polymer/polymer/polymer-element.js';

class Page404 extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          height: 100%;
          display: flex;
          flex-direction: column;
          justify-content: center;
        }
        #content {
          text-align: center;
        }
        a {
          color: white;
        }
      </style>
      <div id="content">
        <h1>Oops you hit a 404.</h1>
        <a href="[[rootPath]]">Head back to home.</a>
      </div>
    `;
  }
}

window.customElements.define('page-404', Page404);
