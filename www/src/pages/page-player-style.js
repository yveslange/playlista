export default `/* <paper-input> */
  paper-input {
    margin-top: -7px;
  }

  #content {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    overflow: auto;
    max-height: 100%;
  }

  #search-bar {
    padding: 0 10px;
  }

  #player-container {
    height: 65px;
    min-height: 65px;
    -webkit-box-shadow: 0px 2px 4px -4px rgba(0,0,0,0.75);
    -moz-box-shadow: 0px 2px 4px -4px rgba(0,0,0,0.75);
    box-shadow: 0px 2px 4px -4px rgba(0,0,0,0.75);
  }

  #playlist {
  }

  .song {
    overflow: hidden;
    font-size: 13px;
    padding: 10px;
    cursor: pointer;
    display: flex;
    align-items: center;
  }
  .song:hover {
    background-color: var(--accent-color);
  }
  .song[playing=""] {
    background-color: var(--default-primary-color);
    color: white;
  }
  .song img {
    margin-right: 5px;
  }
  .song[filtered] {
    display: none;
  }
`;
